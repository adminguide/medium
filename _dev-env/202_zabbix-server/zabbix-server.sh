# If Windows corrupted carriages, to fix, use: sed -i -e 's/\r$//' script.sh
# Update APT cache
sudo apt update 
# Install packages we need to install Docker:
sudo apt install ca-certificates curl gnupg -y
# Import GPG key:
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add docker repository to /etc/apt/sources.list.d:
echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# Update APT cache to get info about Docker packages:
sudo apt update

# Install Docker & Docker Compose packages
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# Run hello-world to check docker is working
sudo docker run hello-world

# Check the Docker Compose version
docker compose version

sudo touch /etc/docker/daemon.json
sudo chown adminguide: /etc/docker/daemon.json
sudo cat >> /etc/docker/daemon.json << EOL
{
  "metrics-addr": "0.0.0.0:9323"
}
EOL
sudo chown root: /etc/docker/daemon.json
sudo systemctl restart docker

git clone https://gitlab.com/adminguide/medium.git
sudo mv ./medium/zabbix-server /opt/zabbix-server
sudo docker compose -f /opt/zabbix-server/docker-compose.yml up -d