# If Windows corrupted carriages, to fix, use: sed -i -e 's/\r$//' script.sh
sudo sed -i 's/#$nrconf{restart} = '"'"'i'"'"';/$nrconf{restart} = '"'"'a'"'"';/g' /etc/needrestart/needrestart.conf
#$nrconf{restart} = 'i';
# Update APT cache
sudo apt update 
# Install packages we need to use Python3:
sudo apt install python3 python3-pip -y
pip3 install woocommerce
pip3 install pylint

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# Register runner
sudo gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token GR13489418MF9sWdnK6dExE682NCV --executor "shell"

# Comment out any uncommentet line in /home/gitlab-runner/.bash_logout to disable console clearing
sudo sed -i 's/^\([^#].*\)/# \1/g' /home/gitlab-runner/.bash_logout