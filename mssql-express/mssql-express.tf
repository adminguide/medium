terraform {
 required_providers {
  proxmox = {
   source = "telmate/proxmox"
   version = "2.9.14"
  }
 }
}

# Proxmox provider configuration
provider "proxmox" {
 # Link to the Proxmox VE API
 pm_api_url = "https://ag-proxmox-ve01.lan:8006/api2/json"

 # API token of the user that will run all operations. This data is sensitive
 pm_api_token_id = "adminguide_tf@pam!tf_token"

 # API token secret, this data is sensitive, too.
 pm_api_token_secret = "f9ec1abb-dc01-4ad7-ab54-1c28782e8a89" # Необходимо подставить свой

 # allow insecure connections to API
 pm_tls_insecure = true
}

# resource type proxmox_vm_qemu named mp4_to_gif_converter
resource "proxmox_vm_qemu" "adminguide-mssql-express" {
 # VM name
 name = "adminguide-mssql-express"

 # CloudInit user
 ciuser = "adminguide"

 # ciuser public SSH key
 sshkeys = <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCv3+ll6eH2H3UltwcqW4308X7X4mNu1PtE0tZsSO1aR4u3CIZg3L2QewJWnRHtHuttYVVF9ZIaN7p1sDAfe9C+MyLqiN0ep3mvY5FlWmOnU0t+hKB1gmKL4CIZGiTotu41s2Y1W3rmEluHISK5lnVqwHAtPXIsdaTZtxRSy1ukr0wk6VYKMKhEsFYgRFW3qUpsiysUjd/1xO0LYpYTZSe3sCZZNM6H5ZjuxjM6wWvhJjoL07NFwYQsBgc+EtZe+La2MoqotUl5Mszx99QLpBUb8MEcs1/dMppT0+43bgWqHR9dMPj57TqvDfT8ZIgzmcz5vKdI0uRm6jQ4oQI6Wx4V adminguide
EOF

 # target Proxmox VE node
 target_node = "ag-proxmox-ve01"

 # CloudInit template that will be cloned
 clone = "u22s-tpl"

 # Select boot order
 boot = "order=scsi0"

 # Enable qemu guest agent
 agent = 1

 os_type = "cloud-init"
 
 cores = 2
 sockets = 1
 memory = 4096

 # CPU type. With the "host" type - maximum compatibility with the host CPU will be set
 cpu = "host"

 # SCSI controller type
 scsihw = "virtio-scsi-pci"

 tags = "Docker,MSSQL Express"

 # scsi0 disk config
 disk {
  slot = 0
  size = "32G"
  type = "scsi"
  storage = "local-lvm" # hdd storage for disk
 }

 # Network adapter configuration
 network {
  model = "virtio"
  bridge = "vmbr0"
 }

 # Configure static IP for network adapter
 ipconfig0 = "ip=192.168.88.123/24,gw=192.168.88.1"
 
 # SSH connection for remote-exec provisioner
 connection {
   host = "192.168.88.123" # IP address of the host where commands will run
   user = "adminguide" # User
   private_key = file("/home/adminguide/.ssh/id_rsa_tf") # Path to private SSH key located on host running Terraform.
   agent = false # Disable ssh-agent authentication
   timeout = "3m"
 }

 # Deliver target file to remote host
 provisioner "file" {
   source   = "mssql-express.sh"
   destination = "/tmp/script.sh"
 }

 # Run command on the remote host
 provisioner "remote-exec" {
   inline = [
  "chmod +x /tmp/script.sh",
  "sh /tmp/script.sh",
   ]
 }
}